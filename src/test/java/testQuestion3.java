///---------------------------------Решение тестового задания №3---------------------

//**************************************************************************************************************
//1. Зайди в любую поисковую систему.
//2. Найди сайт Альфа-Банка через поисковую систему.
//3. Перейти на сайт Альфа-Банк
//4. Перейди в раздел «Вакансии». (ты попадешь на сайт job.alfabank.ru)
//5. На сайте job.alfabank.ru перейди в раздел «о работе в банке»
//6. Внизу страницы есть три абзаца текста. Весь этот текст нужно поместить в файл.
//Требования к именованию файла:
//        a. Имя должно содержать дату и время прогона.
//        b. Имя должно содержать название браузера, в котором совершался прогон.
//        c. Имя должно содержать название поисковой системы.
// *************************************************************************************************************

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class testQuestion3 {

    private WebDriver driver;
    private GoogleMainPage googleMainPage;
    private AlphaMainPage alphaMainPage;

    //тест для поиска:
    private String textForSearch = "Альфа-банк";
    //переменная для относительного пути:
    private String relativePath = System.getProperty("user.dir");
    //Путь для записи файла:
    private String filePath = relativePath+"\\logs\\";


    @Before
    public void setUp() {
        System.setProperty(MainClass.seleniumDriver, MainClass.directoryOfSeleniumDriver);
        driver = new ChromeDriver();
        driver.manage().window().maximize();//максимизируем браузер (не фулскрин)
        driver.get(MainClass.googleMainUrl);//перейти на главную страницу google
        //создаем экземпляр страницы GooogleMainPage через переменную mainPage, что бы в тесте получить доступ к методам класса GoogleMainPage.
        googleMainPage = new GoogleMainPage(driver);
        alphaMainPage = new AlphaMainPage(driver);
    }

    @After
    //Метод для закрытия драйвера:
    public void tearDown() {
        driver.quit();
    }

    //Тест на помещение текста в файл:
    @Test
    public void alphaBankFileTest(){
    //Запоминаем поисковую систему:
    String searchingSystemName = driver.getTitle();
    //заполняем строку поиска и нажимаем ENTER:
    googleMainPage.typeFieldAndENTER(googleMainPage.searchField, textForSearch);
    //передаем в переменную alphasite, найденный URL из поиска:
    String alphaSite = driver.findElement(googleMainPage.alphaBankHttps).getText();
    //переходим на сайт AlphaBank:
    driver.get(alphaSite);
    alphaMainPage.clickerAlphaMainPage(alphaMainPage.vacanciesButton, alphaMainPage.vacanciesButtonName,5);
    //Переходим в раздел 'о работе в банке':
    alphaMainPage.clickerAlphaMainPage(alphaMainPage.aboutBankButton, alphaMainPage.aboutBankButtonName, 5);
    //передаем заголовок и текст в переменные:
    String header = driver.findElement(alphaMainPage.messageHeaderText).getText();
    String bigInfo = driver.findElement(alphaMainPage.bigInfoText).getText();

    //получение имени браузера (таким же способом получают и версию, но в задании этого не требуется):
    Capabilities caps = ((RemoteWebDriver)driver).getCapabilities();
    System.out.println(caps.getBrowserName());
    String browserName = caps.getBrowserName();

    //переменная для именования файла:
    String fileName = MainClass.dataReturner()+browserName+searchingSystemName+".txt";
    //Записываем в файл передавая в метод, необходимые для именования и пути параметры:
    MainClass.fileWriter(header,bigInfo,filePath,fileName);


    }
}