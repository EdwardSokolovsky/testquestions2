///---------------------------------Решение тестового задания №2---------------------

//**************************************************************************************************************
//1. Зайти на yandex.ru
//2. Перейти в маркет, подраздел электроника, мобильные телефоны.
//3. Сделай фильтрацию по смартфонам марки Samsung.
//4. Сделай фильтр по цене от 40 000 рублей.
//5. Возьми имя первого смартфона, запомнить его.
//6. Перейди по ссылке первого смартфона в его описание.
//7. Сравни, что отображаемое имя телефона в описании совпадает с тем, которое ты
//запомнил.
//8. Сделай все тоже самое п.3-7 с разделом наушники, марка beats, с ценой от 17 до
//25 тыс. руб.
// *************************************************************************************************************

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.applet.Main;

import java.util.concurrent.TimeUnit;

public class testQuestion2 {

    private WebDriver driver;
    private YandexMainPage yandexMainPage;
    private MarketMainPage marketMainPage;

    //переменная для фильтра цены SAMSUNG
    private String MobilePriceFrom = "40000";
    //переменные для фильтра цены Beats
    private String BeatsPriceFrom = "17000";
    private String BeatsPriceBefore = "25000";

    @Before
    public void setUp() {
        System.setProperty(MainClass.seleniumDriver, MainClass.directoryOfSeleniumDriver);
        driver = new ChromeDriver();
        driver.manage().window().maximize();//максимизируем браузер (не фулскрин)
        driver.get(MainClass.yandexMainUrl);//перейти на главную страницу Яндекс
        //создаем экземпляр страницы YandexMainPage через переменную mainPage, что бы в тесте получить доступ к методам класса YandexMainPage.
        yandexMainPage = new YandexMainPage(driver);
        marketMainPage = new MarketMainPage(driver);
    }

    @After
    //Метод для закрытия драйвера:
    public void tearDown() {
        driver.quit();
    }

    //Тест сверки названий телефонов SAMSUNG:
    @Test
    public void yamarketSamsungAssertsTest(){
        //переходим на страницу Маркета:
        yandexMainPage.clickerYandexMainPage(yandexMainPage.marketButton, yandexMainPage.marketButtonName,3);
        //отвечаем 'Да' на вопрос о городе:
        marketMainPage.clickerMarketMainPage(marketMainPage.cityPushButton, marketMainPage.cityPushButtonName, 3);
        //Из выпадающего меню 'Электроника' выбираем 'Мобильные телефоны':
        marketMainPage.selectDropDownMenuItem(marketMainPage.electronicsDropDownMenu, marketMainPage.mobilesDropDownMenuItem, marketMainPage.electronicsDropDownMenuName, marketMainPage.mobilesDropDownMenuItemName, 5);
        //заполняем поле 'цена ОТ':
        marketMainPage.typeFieldAndENTER(marketMainPage.priceFromField, MobilePriceFrom);
        //кликаем чек-бокс 'SAMSUNG':
        marketMainPage.clickerMarketMainPage(marketMainPage.checkBoxSamsung, marketMainPage.checkBoxSamsungName, 3);
        MainClass.timeoutX(3000);
        //получаем имя первого телефона и передаем его в переменную:
        String firstMobileFromFilterName = driver.findElement(marketMainPage.firstDeviceInList).getText();
        System.out.println(driver.findElement(marketMainPage.firstDeviceInList).getText());
        //кликаем на первый мобильный телефон:
        marketMainPage.clickerMarketMainPage(marketMainPage.firstDeviceInListImage, firstMobileFromFilterName, 3);
        //передаем в переменную название телефона со страницы телефона:
        String firstMobileNameFromMobilePage = driver.findElement(marketMainPage.firstMobileTextelementOnMobilePage).getText();
        //сравниваем две переменные:
        Assert.assertEquals(firstMobileFromFilterName, firstMobileNameFromMobilePage);
        //P.S. Тест будет успешным, если названия переданные в переменные будут идентичны.
    }

    //Тест сверки названий наушников Beats:
    @Test
    public void yamarketBeatsAssertsTest(){
        //переходим на страницу Маркета:
        yandexMainPage.clickerYandexMainPage(yandexMainPage.marketButton, yandexMainPage.marketButtonName,3);
        //отвечаем 'Да' на вопрос о городе:
        marketMainPage.clickerMarketMainPage(marketMainPage.cityPushButton, marketMainPage.cityPushButtonName, 3);
        //Из выпадающего меню 'Электроника' выбираем 'Наушники':
        marketMainPage.selectDropDownMenuItem(marketMainPage.electronicsDropDownMenu, marketMainPage.headphonesDropDownMenuItem, marketMainPage.electronicsDropDownMenuName, marketMainPage.headphonesDropDownMenuItemName, 5);
        //заполняем поле 'цена ОТ':
        marketMainPage.typeFieldAndENTER(marketMainPage.priceFromField, BeatsPriceFrom);
        //заполняем поле 'цена ДО':
        marketMainPage.typeFieldAndENTER(marketMainPage.priceBeforeField, BeatsPriceBefore);
        //кликаем чек-бокс 'Beats':
        marketMainPage.clickerMarketMainPage(marketMainPage.checkBoxBeats, marketMainPage.checkBoxBeatsName, 3);
        MainClass.timeoutX(3000);
        //получаем имя первых наушников и передаем его в переменную:
        String firstHeadPhonesFromFilterName = driver.findElement(marketMainPage.firstDeviceInList).getText();
        System.out.println(driver.findElement(marketMainPage.firstDeviceInList).getText());
        //кликаем на первые наушники:
        marketMainPage.clickerMarketMainPage(marketMainPage.firstDeviceInListImage, firstHeadPhonesFromFilterName, 3);
        //передаем в переменную название наушников со страницы наушников:
        String firstHeadphonesNameFromMobilePage = driver.findElement(marketMainPage.firstHeadphonesTextelementOnMobilePage).getText();
        //сравниваем две переменные:
        Assert.assertEquals(firstHeadPhonesFromFilterName, firstHeadphonesNameFromMobilePage);
        //P.S. Тест будет успешным, если названия переданные в переменные будут идентичны.
    }
}