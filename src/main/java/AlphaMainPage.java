/*
                                   Главная страница Альфа-банка
*/

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AlphaMainPage {

    static WebDriver driver;

    //Конструктор драйвера:
    public AlphaMainPage(WebDriver driver) {
        this.driver = driver;
    }

    /*

                                     Элементы страницы alfabank.ru

    */

   //Гипперссылка 'Вакансии':
    public final By vacanciesButton = By.xpath("//a[@title='Вакансии']");
   //Имя гипперссылки 'Вакансии':
    public final String vacanciesButtonName = "Вакансии";
    //Гипперссылка 'О работе в банке':
    public final By aboutBankButton = By.xpath("//span[@class='nav_item-link-helper'][text()='О работе в банке']");
    //Имя гипперссылки 'О работе в банке':
    public final String aboutBankButtonName = "О работе в банке";
    //Объект содержащий три абзаца текста:
    public final By bigInfoText = By.xpath("//div[@class='info']");
    //Объект с заголовком:
    public final By messageHeaderText = By.xpath("//div[@class='message']");
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

    //Метод заполнения полей и нажатия ENTER:
    public AlphaMainPage typeFieldAndENTER (By field, String sendText){
        driver.findElement(field).sendKeys(sendText);
        driver.findElement(field).sendKeys(Keys.ENTER);
        return new AlphaMainPage(driver);
    }

    //метод для клика на элементы страницы
    public AlphaMainPage clickerAlphaMainPage (By element, String buttonName, int waitingTime) {
        try {
            WebElement elementWait = (new WebDriverWait(driver, waitingTime))
                    .until(ExpectedConditions.presenceOfElementLocated(element));
            driver.findElement(element).click();
            System.out.println((char) 27 + "[32mКнопка '"+buttonName+"' Успешно нажата");
        } catch (org.openqa.selenium.TimeoutException tex) {
            System.out.println((char) 27 + "[31mНе удалось кликнуть на кнопку '"+buttonName+"', кнопка не отобразилась в течении "+waitingTime+" секунд");
        }
        return new AlphaMainPage(driver);

    }


}
