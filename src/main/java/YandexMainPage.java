/*
                                   Шаблон главной страницы Яндекса
*/

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class YandexMainPage {

    static WebDriver driver;

    //Конструктор драйвера:
    public YandexMainPage(WebDriver driver) {
        this.driver = driver;
    }

    /*

                                     Элементы страницы yandex.ru:

    */

    //Кнопка 'Яндекс маркет':
    public final By marketButton = By.xpath("//*[text()='Маркет'][@data-id='market']");
    public final String marketButtonName = "Маркет";

    //метод для клика на элементы страницы
    public YandexMainPage clickerYandexMainPage (By element, String buttonName, int waitingTime) {
        try {
            WebElement elementWait = (new WebDriverWait(driver, waitingTime))
                    .until(ExpectedConditions.presenceOfElementLocated(element));
            driver.findElement(element).click();
            System.out.println((char) 27 + "[32mКнопка '"+buttonName+"' Успешно нажата");
        } catch (org.openqa.selenium.TimeoutException tex) {
            System.out.println((char) 27 + "[31mНе удалось кликнуть на кнопку '"+buttonName+"', кнопка не отобразилась в течении "+waitingTime+" секунд");
        }
        return new YandexMainPage(driver);

    }

}
