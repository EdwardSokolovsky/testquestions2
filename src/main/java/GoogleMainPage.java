/*
                                   Шаблон главной страницы Google
*/

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleMainPage {

    static WebDriver driver;

    //Конструктор драйвера:
    public GoogleMainPage(WebDriver driver) {
        this.driver = driver;
    }

    /*

                                     Элементы страницы google.ru:

    */

    //Строка поиска:
    public final By searchField = By.id("lst-ib");
    //Строка с адресом Альфа-Банка:
    public final By alphaBankHttps = By.xpath("//cite[@class='iUh30'][text()='https://alfabank.ru/']");

    //метод для клика на элементы страницы
    public GoogleMainPage clickerGoogleMainPage (By element, String buttonName, int waitingTime) {
        try {
            WebElement elementWait = (new WebDriverWait(driver, waitingTime))
                    .until(ExpectedConditions.presenceOfElementLocated(element));
            driver.findElement(element).click();
            System.out.println((char) 27 + "[32mКнопка '"+buttonName+"' Успешно нажата");
        } catch (org.openqa.selenium.TimeoutException tex) {
            System.out.println((char) 27 + "[31mНе удалось кликнуть на кнопку '"+buttonName+"', кнопка не отобразилась в течении "+waitingTime+" секунд");
        }
        return new GoogleMainPage(driver);

    }

    //Метод заполнения полей и нажатия ENTER:
    public GoogleMainPage typeFieldAndENTER (By field, String sendText){
        driver.findElement(field).sendKeys(sendText);
        driver.findElement(field).sendKeys(Keys.ENTER);
        return new GoogleMainPage(driver);
    }

}
