/*
                                   Главная страница Яндекс.Маркета
*/

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MarketMainPage {

    static WebDriver driver;

    //Конструктор драйвера:
    public MarketMainPage(WebDriver driver) {
        this.driver = driver;
    }

    /*

                                     Элементы страницы https://market.yandex.ru/:

    */


    //Кнопка 'Электроника'
    public final By electronicsButton = By.xpath("//*[text()='Электроника'][@class='link topmenu__link']");
    //Имя кнопки 'Электроника'
    public final String electronicsButtonName = "Электроника";
    //Кнопка 'Да' на всплывающем пуше 'Ваш город Москва?':
    public final By cityPushButton = By.xpath("//*[@class='n-region-notification__actions layout layout_display_flex']/child::div[1]");
    //Имя кнопки 'Да, мой город Москва'
    public final String cityPushButtonName = "Да, мой город Москва";
    //Выпадающее меню 'Электроника':
    public final By electronicsDropDownMenu = By.linkText("Электроника");
    //Имя дроп-меню электроники:
    public final String electronicsDropDownMenuName = "Выпадающее меню 'Электроника'";
    //итем Мобильные телефоны меню 'Электроника':
    public final By mobilesDropDownMenuItem = By.linkText("Мобильные телефоны");
    //Имя дроп-меню телефоны:
    public final String mobilesDropDownMenuItemName = "Мобильные телефоны (итем выпадающего меню 'Электроника')";
    //итем Наушники меню 'Электроника':
    public final By headphonesDropDownMenuItem = By.linkText("Наушники");
    //Имя дроп-меню наушники:
    public final String headphonesDropDownMenuItemName = "Наушники (итем выпадающего меню 'Электроника')";
    //поле фильтра 'Цена ОТ':
    public final By priceFromField = By.xpath("//*[@class='_2yK7W3SWQ- _1d02bPcWht'][@type='text']");
    //поле фильтра 'Цена ДО':
    public final By priceBeforeField = By.xpath("//*[@class='_2yK7W3SWQ- _1f2usTwyAs'][@type='text']");
    //Первый девайс в списке, после фильтра:
    public final By firstDeviceInList = By.xpath("(//div[@class='n-snippet-cell2__title'])[1]");
    //Изображение первого девайса в списке, после фильтра:
    public final By firstDeviceInListImage = By.xpath("(//img[@class='image'])[1]");

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-РАЗДЕЛ МОБИЛЬНЫЕ ТЕЛЕФОНЫ-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //Чек-бокс SAMSUNG:
    public final By checkBoxSamsung = By.xpath("//*[@class='LhMupC0dLR'][@data-reactid='135']");
    //Имя чек-бокса SAMSUNG:
    public final String checkBoxSamsungName = "чек-бокс 'SAMSUNG'";
    //Элемент с текстом на странице первого телефона в списке:
    public final By firstMobileTextelementOnMobilePage = By.xpath("//h1[@class='title title_size_28 title_bold_yes']");

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-РАЗДЕЛ НАУШНИКИ*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
    //Чек-бокс Beats:
    public final By checkBoxBeats = By.xpath("//*[@class='LhMupC0dLR'][@data-reactid='79']");
    //Имя чек-бокса Beats:
    public final String checkBoxBeatsName = "чек-бокс 'Beats'";
    //Элемент с текстом на странице наушников:
    public final By firstHeadphonesTextelementOnMobilePage = By.xpath("//h1[@class='title title_size_28 title_bold_yes']");



    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

    //Метод заполнения полей и нажатия ENTER:
    public MarketMainPage typeFieldAndENTER (By field, String sendText){
        driver.findElement(field).sendKeys(sendText);
        driver.findElement(field).sendKeys(Keys.ENTER);
        return new MarketMainPage(driver);
    }

    //метод для клика на элементы страницы
    public MarketMainPage clickerMarketMainPage (By element, String buttonName, int waitingTime) {
        try {
            WebElement elementWait = (new WebDriverWait(driver, waitingTime))
                    .until(ExpectedConditions.presenceOfElementLocated(element));
            driver.findElement(element).click();
            System.out.println((char) 27 + "[32mКнопка '"+buttonName+"' Успешно нажата");
        } catch (org.openqa.selenium.TimeoutException tex) {
            System.out.println((char) 27 + "[31mНе удалось кликнуть на кнопку '"+buttonName+"', кнопка не отобразилась в течении "+waitingTime+" секунд");
        }
        return new MarketMainPage(driver);

    }

    //метод для выбора итема из любого выпадающего меню страницы :
    public MarketMainPage selectDropDownMenuItem (By menu, By menuItem, String menuName, String menuItemName, int waitingTime) {

        WebDriverWait wait = new WebDriverWait(driver, waitingTime);
        wait.until(ExpectedConditions.presenceOfElementLocated(menu));

        WebElement electronicsMenu = driver.findElement(menu);
        Actions builder = new Actions(driver);
        builder.moveToElement(electronicsMenu).build().perform();

        wait.until(ExpectedConditions.presenceOfElementLocated(menuItem));

        WebElement menuOption = driver.findElement(menuItem);
        builder.moveToElement(menuOption).click().build().perform();
        System.out.println((char) 27 + "[32mКнопка '"+menuItemName+"' из меню '"+menuName+"' успешно нажата");
        return new MarketMainPage(driver);
    }

}
