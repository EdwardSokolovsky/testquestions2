import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

class MainClass {

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-
                                                                MainClass
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*/


    static WebDriver driver;
    private YandexMainPage mainPage;
/*
         --------------- ------- -----   Задаваемые параметры для тестирования:    ---------------------------
*/

    //Проперти, драйвер, путь к директории где лежит драйвер:
    public static String seleniumDriver = "webdriver.chrome.driver";
    public static String driverPath = System.getProperty("user.dir");
    public static String directoryOfSeleniumDriver = driverPath+"\\driver\\chromedriver.exe";


    //URL'ы тестируемых сайтов:
    public static String yandexMainUrl = "https://yandex.ru/";
    public static String googleMainUrl = "https://www.google.ru/";


    public static void timeoutX(long milliseconds) {
        try {
            Thread.sleep(milliseconds);//X секунд
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Запись в файл:
    public static void fileWriter  (String header, String bigInfo, String filePath, String fileName) {

        try {
            String str = header+"\n"+bigInfo;
            File newTextFile = new File(filePath+fileName);

            FileWriter fw = new FileWriter(newTextFile);
            fw.write(str);
            fw.close();

        } catch (IOException iox) {
            //block for exeption
            iox.printStackTrace();
        }
    }

    //Метод для получения текущей даты и времени:
    public static String dataReturner (){
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd HH_mm_ss");
        Calendar cal = Calendar.getInstance();
        System.out.println(dateFormat.format(cal.getTime()));
        return dateFormat.format(cal.getTime());
    }


    public static void main(String... args) {

        System.setProperty(seleniumDriver, directoryOfSeleniumDriver);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(MainClass.yandexMainUrl);

    }





}